/*
 * Copyright (c) 2023 by frostime. All Rights Reserved.
 * @Author       : frostime
 * @Date         : 2023-12-13 01:00:32
 * @FilePath     : /src/components/layout/row.ts
 * @LastEditTime : 2023-12-13 01:04:54
 * @Description  : 
 */
class Column {
    element: HTMLElement;

    constructor(element?: HTMLElement) {
        if (element === undefined) {
            element = document.createElement('div');
        }
        this.element = element;
        this.element.classList.add('fn__flex');
    }

    addSpace() {
        const span = document.createElement('span');
        span.className = 'fn__space';
        this.element.appendChild(span);
        return this;
    }

}