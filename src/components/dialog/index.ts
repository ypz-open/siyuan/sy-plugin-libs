/*
 * Copyright (c) 2023 by frostime. All Rights Reserved.
 * @Author       : frostime
 * @Date         : 2023-12-12 12:25:17
 * @FilePath     : /src/components/dialog/index.ts
 * @LastEditTime : 2023-12-13 01:10:08
 * @Description  : 
 */
import { Dialog, IObject } from "siyuan";


export class TypographyDialog extends Dialog {

    eleContainer: HTMLElement;

    constructor(options: {
        title?: string,
        transparent?: boolean,
        markdown?: string,
        typodom?: string,
        content?: string,
        width?: string
        height?: string,
        destroyCallback?: (options?: IObject) => void,
        disableClose?: boolean,
        disableAnimation?: boolean,
    }) {
        if (options.content === undefined) {
            if (options.typodom === undefined) {
                const lute = window.Lute!.New();
                options.typodom = lute.Md2HTML(options.markdown);
            }
            options.content = `
            <div id="dialog" class="b3-typography" style="margin: 2rem; font-size: 1rem">
                <div id="dialogContent" style="font-size: 1rem">
                    ${options.typodom}
                </div>
            </div>`;
            options.markdown = undefined;
            options.typodom = undefined;
        }

        //@ts-ignore
        super(options);
        this.eleContainer = this.element.querySelector('.b3-dialog__container')!;
    }

    setSize(size: { width?: string; height?: string }) {
        let ele: HTMLElement = this.element.querySelector('.b3-dialog__container')!;
        ele.style.width = size.width ?? ele.style.width;
        ele.style.height = size.height ?? ele.style.height;
    }

    setFont(size: string) {
        let ele: HTMLElement = this.element.querySelector('#dialogContent')!;
        ele.style.fontSize = size;
    }
}

export const confirmDialog = (title: string, text: string, confirm?: () => void, cancel?: () => void) => {
    const dialog = new Dialog({
        title,
        content: `<div class="b3-dialog__content">
    <div class="ft__breakword">${text}</div>
</div>
<div class="b3-dialog__action">
    <button class="b3-button b3-button--cancel">${window.siyuan.languages.cancel}</button><div class="fn__space"></div>
    <button class="b3-button b3-button--text" id="confirmDialogConfirmBtn">${window.siyuan.languages.confirm}</button>
</div>`,
        width: "520px",
    });
    const btnsElement = dialog.element.querySelectorAll(".b3-button");
    btnsElement[0].addEventListener("click", () => {
        if (cancel) {
            cancel();
        }
        dialog.destroy();
    });
    btnsElement[1].addEventListener("click", () => {
        if (confirm) {
            confirm();
        }
        dialog.destroy();
    });
    let div: HTMLDivElement = dialog.element.querySelector(".b3-dialog__container") as HTMLDivElement;
    div.style.maxHeight = "50%";
};

export const inputDialog = (title: string, confirm?: (text: string) => void, cancel?: () => void) => {
    const dialog = new Dialog({
        title,
        content: `<div class="b3-dialog__content">
    <div class="ft__breakword"><textarea class="b3-text-field fn__block" style="height: 100%;"></textarea></div>
</div>
<div class="b3-dialog__action">
    <button class="b3-button b3-button--cancel">${window.siyuan.languages.cancel}</button><div class="fn__space"></div>
    <button class="b3-button b3-button--text" id="confirmDialogConfirmBtn">${window.siyuan.languages.confirm}</button>
</div>`,
        width: "520px",
    });
    const target = dialog.element.querySelector(".b3-dialog__content>div.ft__breakword>textarea") as HTMLTextAreaElement;
    const btnsElement = dialog.element.querySelectorAll(".b3-button");
    btnsElement[0].addEventListener("click", () => {
        if (cancel) {
            cancel();
        }
        dialog.destroy();
    });
    btnsElement[1].addEventListener("click", () => {
        if (confirm) {
            confirm(target.value);
        }
        dialog.destroy();
    });
};

