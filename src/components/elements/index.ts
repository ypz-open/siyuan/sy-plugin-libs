export * from './input';

export const typography = (markdown: string) => {
    const lute = window.Lute!.New();
    let content = lute.Md2HTML(markdown);
    const html = `
    <div class="item__readme b3-typography">
        ${content}
    </div>
    `;
    const div = document.createElement('div');
    div.innerHTML = html;

    div['setFontSize'] = (size: string) => {
        div.style.fontSize = size;
    }

    return div;
}
