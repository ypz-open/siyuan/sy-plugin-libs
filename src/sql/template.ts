
const postProcess = (sql: string, orderBy?: string, limit?: number) => {
    sql = sql.trim();
    if (sql.endsWith(';')) {
        sql = sql.substring(0, sql.length - 1).trim();
    }
    if (orderBy) {
        sql += ` order by ${orderBy}`;
    }
    if (limit) {
        sql += ` limit ${limit}`;
    }
    return sql;
}


const conditionBuilder = (condition: {
    match?: Tmatch<any>,
    defaultOp?: TmatchOp,
    conditions?: string[]
}) => {
    let conds: string[] = [];
    let defaultOp = condition.defaultOp;
    if (condition.match) {
        for (let key in condition.match) {
            let value = condition.match[key];
            let op = defaultOp;
            if (Array.isArray(value)) {
                op = value[0];
                value = value[1];
            }
            conds.push(`${key} ${op} '${value}'`);
        }
    }
    if (condition.conditions) {
        conds.push(...condition.conditions);
    }
    return conds;
}


type TmatchOp = '=' | 'like' | 'in';
type TmatchValue = string | number | boolean;
type TmatchRight = TmatchValue | [TmatchOp, TmatchValue];
type Tmatch<T> = {
    [K in keyof T]?: TmatchRight;
}

interface ISelectCondition<T> {
    result?: string;

    match?: Tmatch<T>;

    defaultOp?: TmatchOp;
    conditions?: string[]; // condition string, example: `B.id = 'xxx'`, `A.key = 'xxx'`

    orderBy?: string;
    limit?: number;
}

/**
 * 创建一个针对 blocks 表的查询语句 | Create a query statement for the blocks table
 * @returns 
 */
export const selectBlocks = (condition: ISelectCondition<Block> = { result: '*', defaultOp: '=' }) => {
    let sql = `select ${condition.result} from blocks where `;
    let conditions: string[] = conditionBuilder(condition);
    sql += conditions.join(' and ');
    return postProcess(sql, condition?.orderBy, condition?.limit);
}


/**
 * 创建一个针对 refs 表的查询语句 | Create a query statement for the refs table
 * @returns 
 */
export const selectRefs = (condition: ISelectCondition<Ref> = { result: '*', defaultOp: '=' }) => {
    let sql = `select ${condition.result} from refs where `;
    let conditions: string[] = conditionBuilder(condition);
    sql += conditions.join(' and ');
    return postProcess(sql, condition?.orderBy, condition?.limit);
}


/**
 * 创建一个针对 attributes 表的查询语句 | Create a query statement for the attributes table
 * @returns 
 */
export const selectAttributes = (condition: ISelectCondition<Attribute> = { result: '*', defaultOp: '=' }) => {
    let sql = `select ${condition.result} from attributes where `;
    let conditions: string[] = conditionBuilder(condition);
    sql += conditions.join(' and ');
    return postProcess(sql, condition?.orderBy, condition?.limit);
}


/**
 * 创建一个针对 blocks 表的查询语句，查询结果包含 refs 表的内容 | Create a query statement for the blocks table, the query result contains the contents of the refs table
 * 
 * In this template, `B` is the alias of the `blocks` table, and `R` is the alias of the `refs` table.
 * 
 * Note: match in condition will be ignored
 */
export const selectBlockJoinRef = (
    condition: ISelectCondition<Block | Attribute> = { result: 'B.*', defaultOp: '=' }
) => {
    let sql = `select ${condition.result} from blocks as B join refs as R on B.id = R.def_block_id where `;
    condition.match = undefined;  // ignore match
    let conditions = conditionBuilder(condition);
    sql += conditions.join(' and ');
    return postProcess(sql, condition.orderBy, condition.limit);
}

/**
 * 创建一个针对 blocks 表的查询语句，查询结果包含 attributes 表的内容 | Create a query statement for the blocks table, the query result contains the contents of the attributes table
 * 
 * In this template, `B` is the alias of the `blocks` table, and `A` is the alias of the `attributes` table.
 * 
 * Note: match in condition will be ignored
 */
export const selectBlockJoinAttribute = (
    condition: ISelectCondition<Block | Attribute> = { result: 'B.*', defaultOp: '=' }
) => {
    let sql = `select ${condition.result} from blocks as B join attributes as A on B.id = A.block_id where `;
    condition.match = undefined;  // ignore match
    let conditions = conditionBuilder(condition);
    sql += conditions.join(' and ');
    return postProcess(sql, condition.orderBy, condition.limit);
}

/***************************** Query **********************************/


/**
 * 根据 blockId 查询 block | Query block by blockId
 * @param blockIds
 * @param orderBy
 * @param limit
 */
export const queryBlocks = (blockIds: string | string[], orderBy?: string, limit?: number) => {
    let sql: string;
    if (Array.isArray(blockIds)) {
        let idList = blockIds.map((id) => `"${id}"`);
        return selectBlocks({ match: { id: ['in', `(${idList.join(",")})`] }, orderBy, limit });
    } else {
        return selectBlocks({ match: { id: blockIds }, orderBy, limit });
    }
}


/**
 * 根据 blockId 查询 引用她的 block | Query blocks that reference her by blockId
 * @param blockId 
 * @param orderBy 
 * @param limit 
 * @returns 
 */
export const queryRefBlock = (blockId: string, orderBy?: string, limit?: number) => {
    return selectBlockJoinRef({conditions: [`R.def_block_id = '${blockId}'`], orderBy, limit});
}


/**
 * 查询指定书签的 block | Query blocks of the specified bookmark
 * @param bookmark 
 * @param orderBy 
 * @param limit 
 * @returns 
 */
export const queryBookmarkBlock = (bookmark: string, orderBy?: string, limit?: number) => {
    return selectBlockJoinAttribute({conditions: [`A.name = 'bookmark'`, `A.value = '${bookmark}'`], orderBy, limit});
}

